﻿using System;
using System.IO;
using System.ServiceProcess;
using Datalogics.PDFL;

namespace PrintPDF
{
    partial class Service1 : ServiceBase
    {
        const string publicFolderPath = @"C:\Users\Public\RamBase\printpdf\";
        FileInfo finfo = null;

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            Logger.Log("Starting Service");

            string printer = string.Empty;
            string pdfFile = string.Empty;
            string crashpdfFile = string.Empty;
            int noprints = 1;

            string configFilePath = publicFolderPath + "print.txt";
            Logger.Log("Config file path: " + configFilePath);


            //while (true)
            //{
            try
            {
                using (StreamReader reader = new StreamReader(configFilePath))
                {
                    printer = reader.ReadLine();
                    pdfFile = reader.ReadLine();
                    crashpdfFile = reader.ReadLine();

                    //string tempnoprints = reader.ReadLine();
                    //if (!string.IsNullOrEmpty(tempnoprints))
                    //    noprints = int.Parse(tempnoprints);

                    
                }

                if (string.IsNullOrEmpty(pdfFile))
                {
                    Logger.Log("Configure printer name and PDF file path in each line in the print.txt file");
                    return;
                }

                if (string.IsNullOrEmpty(pdfFile))
                    pdfFile = @"C:\Users\Public\RamBase\test.pdf";
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message + " " + ex.StackTrace);
            }

            finfo = new FileInfo(pdfFile);

            print(crashpdfFile, printer);

            for (int i = 0; i < 10; i++)
            {
                print(pdfFile, printer);

            }

        }

        static void print(string pdfFile, string printer)
        {
            try
            {
                Spire.Pdf.PdfDocument pdfDocument = new Spire.Pdf.PdfDocument();
                pdfDocument.LoadFromFile(pdfFile);
                pdfDocument.PageScaling = Spire.Pdf.PdfPrintPageScaling.ActualSize;

                pdfDocument.PrinterName = printer;
                var printDocument = pdfDocument.PrintDocument;
                printDocument.PrintPage += printDocument_PrintPage;

                Logger.Log("Printing PDF filename: " + pdfFile);
                Logger.Log("Before printing");
                printDocument.Print();
                Logger.Log("Printed Successfully");
                Logger.Log("");
            }
            catch (Exception ex)
            {
                Logger.Log("Exception occurred while printing using Spire.Pdf" + ex.Message);
            }
        }

        static void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Logger.Log("[SPIRE.PDF] Printing page: " + sender);
        }

        
    }
}
