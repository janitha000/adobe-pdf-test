﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace PrintPDF
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

#if DEBUG
            Service1 myService = new Service1();
            myService.OnDebug();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            try
            {
                Logger.Log("Starting service ");
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                Logger.Log("Error on starting service " + ex.Message + " " + ex.StackTrace);
            }
#endif
        }
    }
}
